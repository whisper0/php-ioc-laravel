<?php

namespace App\Containers;

use Psr\Container\ContainerInterface;

class AppContainer implements ContainerInterface
{
    protected $instances = [];

    public function get($id)
    {
        if ($this->has($id)) {
            return $this->instances[$id];
        }

        $instance = $this->createObject($id);

        $this->instances[$id] = $instance;

        return $instance;
    }

    public function has($id)
    {
        return isset($this->instances[$id]);
    }

    public function createObject($className)
    {
        if(!class_exists($className))
        {
            throw new \Exception("Class $className does not exist");
        }

        $reflectionClass = new \ReflectionClass($className);

        if($reflectionClass->getConstructor() == null)
            return new $className;
        
        $parameters = $reflectionClass->getConstructor()->getParameters();

        $dependencies = $this->buildDependencies($parameters);

        return $reflectionClass->newInstanceArgs($dependencies);
    }

    public function buildDependencies($parameters)
    {
        $dependencies = [];

        foreach($parameters as $parameter)
        {
            $dependencies[] = $this->createObject($parameter->getClass()->getName());
        }

        return $dependencies;
    }
}
